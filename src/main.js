import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import Hotel from './components/Pages/Hotels.vue';
import Puts from './components/Main/Puts.vue';
import SingleHotel from './components/Pages/SingleHotel.vue'; 
import PutList from './components/Pages/PutList.vue'
import Tours from './components/Pages/Tours.vue'
import Guides from './components/Pages/Guides.vue'
import SinglePut from './components/Pages/SinglePut.vue'
import SingleTour from './components/Pages/SingleTour.vue'
import SingleGuid from './components/Pages/SingleGuid.vue'
import Agents from './components/Pages/AgentList.vue'
import SingleAgent from './components/Pages/SingleAgent.vue'

const routes = [
    {
      path: '/puts',
      component: Puts
    }, 
    {
      path: '/hotels',
      component: Hotel
    },
    {
      path: '/single-hotel',
      component: SingleHotel
    }, 
    {
      path: '/putlist',
      component: PutList
    },
    {
      path: '/tours',
      component: Tours
    },
    {
      path: '/guides',
      component: Guides
    },
    {
      path: '/single-put',
      component: SinglePut
    },
    {
      path: '/single-guid',
      component: SingleGuid
    },
    {
      path: '/single-tour',
      component: SingleTour
    },
    {
      path: '/agents',
      component: Agents
    },
    {
      path: '/single-agent',
      component: SingleAgent
    },
];

Vue.config.productionTip = false
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  routes 
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
